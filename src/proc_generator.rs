use components::*;
use fuss::Simplex;
use rayon::prelude::*;
use resources::{Map, PositionLookup, TileType};
use specs::prelude::*;
use specs::saveload::MarkedBuilder;
use specs::saveload::U64Marker;
use utils::*;

pub fn generate_dungeon_vegetation(world: &mut World) {
    let (mut map, position_lookup, collidables) = (
        world.write_resource::<Map>(),
        world.read_resource::<PositionLookup>(),
        world.read_storage::<CollidableMarker>(),
    );

    let positions = generate_simplex(&*map, 1, 0.8, &|tile, pos| {
        if let TileType::Floor = tile {
            !collision_at(pos, &map, &position_lookup, &collidables)
        } else {
            false
        }
    });
    drop(position_lookup);
    drop(collidables);

    for pos in positions.into_iter() {
        map[pos] = TileType::Grass;
    }
}

pub fn generate_chests(world: &mut World) {
    let (map, position_lookup, collidables) = (
        world.read_resource::<Map>(),
        world.read_resource::<PositionLookup>(),
        world.read_storage::<CollidableMarker>(),
    );

    let positions = generate_simplex(&*map, 1, 0.95, &|tile, pos| {
        if let TileType::Floor = tile {
            !collision_at(pos, &map, &position_lookup, &collidables)
        } else {
            false
        }
    });
    drop(map);
    drop(position_lookup);
    drop(collidables);

    for pos in positions.into_iter() {
        world
            // TODO: Dunno why this is necessary, create_entity spits out 'already immutably borrowed' errors, but...I drop the references? Why the errors?
            .create_entity_unchecked()
            .with(DisplayName("Chest".into()))
            .with(pos)
            .with(Health(2.0))
            .with(CombatStats {
                attack: 0.0,
                defense: 0.0,
            })
            .with(Loot(30))
            .with(GameSprite::Chest)
            .marked::<U64Marker>()
            .build();
    }
}

pub fn generate_enemies(world: &mut World) {
    let (map, position_lookup, collidables) = (
        world.read_resource::<Map>(),
        world.read_resource::<PositionLookup>(),
        world.read_storage::<CollidableMarker>(),
    );
    use rand::rngs::SmallRng;
    use rand::Rng;
    use rand::SeedableRng;
    let mut rng = SmallRng::from_seed(map.seed);

    let positions = generate_simplex(&*map, 2, 0.86, &|tile, pos| {
        if let TileType::Floor = tile {
            !collision_at(pos, &map, &position_lookup, &collidables)
        } else {
            false
        }
    });
    drop(map);
    drop(position_lookup);
    drop(collidables);

    for pos in positions.into_iter() {
        world
            // TODO: Dunno why this is necessary, create_entity spits out 'already immutably borrowed' errors, but...I drop these? Why the errors?
            .create_entity_unchecked()
            .with(DisplayName("Enemy".into()))
            .with(pos)
            .with(Health(20.0))
            .with(CombatStats {
                attack: 3.0,
                defense: 2.0,
            })
            .with(Loot(5))
            .with(CollidableMarker)
            .with(GameSprite::Enemy)
            .with(EnemyMarker)
            .marked::<U64Marker>()
            .with(LookDirection::North)
            .with(NpcActivities(vec![NpcState::Roaming]))
            .with(NpcVelocity(rng.gen_range(60, 130)))
            .build();
    }
}

/// Generate a list of positions based on simplex noise and the map
///
/// * `map` - The `Map`, also containing the seed used
/// * `seed_offset` - This is added (wrapping) to the seed; used for variation between different generators using the same basic seed
/// * `simplex_cutoff` - Value between `0.0` and `1.0`, if noise at y,x is bigger than this value, the position is used
/// * `predicate` - custom predicate that can be used for further validation; needs to be `Send+Sync` because rayon parallel iterators are used in the iteration
fn generate_simplex<'a, F>(
    map: &Map,
    seed_offset: usize,
    simplex_cutoff: f32,
    predicate: F,
) -> Vec<Position>
where
    F: Send + Sync + Fn(TileType, Position) -> bool,
{
    let mut seed: Vec<usize> = map.seed.clone().into_iter().map(|x| *x as usize).collect();
    seed[0] = seed[0].wrapping_add(seed_offset);
    let simplex = Simplex::from_seed(seed);

    map.tiles
        .par_iter()
        .enumerate()
        .flat_map(|(y, row)| {
            row.iter()
                .enumerate()
                .filter_map(|(x, tile)| {
                    let pos = Position {
                        y: y as i32,
                        x: x as i32,
                    };
                    if predicate(*tile, pos) {
                        if simplex.noise_2d(y as f32, x as f32) > simplex_cutoff {
                            Some(pos)
                        } else {
                            None
                        }
                    } else {
                        None
                    }
                }).collect::<Vec<Position>>()
        }).collect()
}
