// Seems like rustc compiles otherwise, weird
#![allow(unknown_lints)]
#![deny(clippy)]
#![deny(clippy_correctness)]
#![warn(clippy_nursery)]
#![warn(clippy_complexity)]
#![warn(clippy_perf)]
// Just makes code more readable sometimes imho (maybe remove after GameSystemState is refactored to an enum)
#![allow(bool_comparison)]
// Doesn't play well with Specs, the SystemData Associated Types in the System trait are always "complex"
#![allow(type_complexity)]
#![feature(extern_prelude)]

extern crate fuss;
extern crate radiant_rs;
extern crate radiant_utils;

extern crate rand;
extern crate rayon;
extern crate specs;

#[macro_use]
extern crate serde_derive;
extern crate serde;

#[macro_use]
extern crate slog;
extern crate serdeconv;
extern crate sloggers;

extern crate failure;

extern crate ron;

#[macro_use]
mod utils;

mod components;
mod proc_generator;
mod resources;
mod systems;

use radiant_rs::{blendmodes, Color, Display, Input, InputId, Layer, Renderer, Sprite};

use slog::Logger;

use proc_generator::*;
use std::collections::HashSet;

use specs::{
    prelude::{Dispatcher, DispatcherBuilder},
    Builder, RunNow, World,
};

use components::*;
use resources::*;
use systems::*;

const SEED: [u8; 16] = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
const NUMBER_OF_CAVES: u32 = 10;
const CAVE_SIZE: u32 = 150;

fn main() {
    let logger = utils::logger_from_config();

    debug!(logger, "Starting up...");

    run(&logger);
}

fn register_resources<'a>(logger: &Logger, world: &mut World) -> Dispatcher<'a, 'a> {
    world.register::<Position>();
    world.register::<LookDirection>();
    world.register::<CollidableMarker>();
    world.register::<Score>();
    world.register::<DisplayName>();
    world.register::<CombatStats>();
    world.register::<Loot>();
    world.register::<Health>();
    world.register::<PlayerMarker>();
    world.register::<GameSprite>();
    world.register::<EnemyMarker>();
    world.register::<NpcActivities>();
    world.register::<NpcVelocity>();
    world.register::<U64Marker>();
    world.add_resource(U64MarkerAllocator::new());
    world.add_resource(Seed(SEED));
    world.add_resource(PortalMappings::default());

    world.add_resource(CurrentMap(0));

    let map = Map::generate_surface(
        80,
        NUMBER_OF_CAVES,
        &SEED,
        &mut *world.write_resource::<PortalMappings>(),
    );
    map.print();
    world.add_resource(map);
    world.add_resource(PositionLookup::default());
    world.add_resource(InputCommandSet::default());
    world.add_resource(GameState {
        step: Step::WaitingForInput,
        time_delta: 0.0,
    });

    for i in 10..20 {
        Map::generate_cave(50, 500, &SEED, i, Position { y: 0, x: 0 }).print();
    }

    let mut position_sync_system = PositionLookupSyncSystem::default();
    position_sync_system.setup(&mut world.res);

    use specs::saveload::*;
    let player_entity = world
        .create_entity()
        .with(PlayerMarker)
        .with(DisplayName("Player".into()))
        .with(Position { x: 25, y: 2 })
        .with(LookDirection::North)
        .with(Health(5.0))
        .with(CombatStats {
            attack: 5.0,
            defense: 2.0,
        }).with(Score(0))
        .with(GameSprite::Player)
        .marked::<U64Marker>()
        .build();

    world.add_resource(PlayerEntity(player_entity));

    debug!(*logger, "Initialising GameData; setting up systems...");
    let mut builder = DispatcherBuilder::new();
    builder.add(InputWalkSystem::default(), "input_walk_system", &[]);
    builder.add(
        position_sync_system,
        "position_lookup_sync_system",
        &["input_walk_system"],
    );
    builder.add(
        InputLookDirectionSystem::default(),
        "input_look_direction_system",
        &[],
    );
    builder.add(InputFightSystem::default(), "input_fight_system", &[]);
    builder.add(EnemiesFightSystem::default(), "enemies_fight_system", &[]);
    builder.add(EnemiesWalkSystem::default(), "enemies_walk_system", &[]);
    debug!(*logger, "5");
    builder.build()
}

fn run(logger: &Logger) {
    info!(logger, "Starting game!");
    let mut world = World::new();
    let mut dispatcher = register_resources(logger, &mut world);
    let mut deletion_dispatcher = {
        let mut builder = DispatcherBuilder::new();
        builder.add(DeletionSystem::default(), "deletion_system", &[]);
        builder.build()
    };

    let display = Display::builder()
        .dimensions((800, 800))
        .vsync()
        .title("Roguers")
        .build()
        .unwrap();
    let input = Input::new(&display);
    let renderer = Renderer::new(&display).unwrap();
    // let dungeon_tiles_sprites = Sprite::from_file(&renderer.context(), "dungeon_tiles_10x6.png").unwrap();
    // let item_sprites = Sprite::from_file(&renderer.context(), "item_sprites_9x10.png").unwrap();
    let sprite = Sprite::from_file(&renderer.context(), "sprites_8x8.png").unwrap();
    let layer = Layer::new((400.0, 400.0));
    layer.set_blendmode(blendmodes::ADD);

    let mut time_step = 0.0;

    radiant_utils::renderloop(|frame| {
        // HACK
        let mut generate_stuff = false;

        if input.pressed(InputId::T, false) {
            let mut dispatcher = DispatcherBuilder::new();
            dispatcher.add(Serialize::default(), "serialize", &[]);
            let mut dp = dispatcher.build();
            dp.dispatch(&world.res);
        }

        if input.pressed(InputId::U, false) {
            let mut dispatcher = DispatcherBuilder::new();
            dispatcher.add(Deserialize::default(), "deserialize", &[]);
            dispatcher.add(
                PostDeserialize::default(),
                "post_deserialize",
                &["deserialize"],
            );
            let mut dp = dispatcher.build();
            dp.dispatch(&world.res);
        }

        match world.read_resource::<GameState>().step.clone() {
            // TODO: could probably move these into systems for better readability
            Step::EnteringCave(to_map, new_position) => {
                let mut seed = SEED.clone();
                seed[0] += to_map as u8;

                let old_position = world
                    .read_storage::<Position>()
                    .get(world.read_resource::<PlayerEntity>().0)
                    .expect("player has no position!")
                    .clone();
                let map = Map::generate_cave(CAVE_SIZE, 1000, &seed, to_map as u8, old_position);

                *world.write_resource::<Map>() = map;
                let new_position = match new_position {
                    Some(p) => p,
                    None => Position {
                        y: (CAVE_SIZE / 2) as i32,
                        x: (CAVE_SIZE / 2) as i32,
                    },
                };
                *world
                    .write_storage::<Position>()
                    .get_mut(world.read_resource::<PlayerEntity>().0)
                    .expect("player has no position!") = new_position;

                *world.write_resource::<CurrentMap>() = CurrentMap(to_map);

                generate_stuff = true;
                deletion_dispatcher.dispatch(&world.res);
            }
            Step::EnteringSurface(surface_position) => {
                let map = Map::generate_surface(
                    80,
                    NUMBER_OF_CAVES,
                    &SEED,
                    &mut *world.write_resource::<PortalMappings>(),
                );
                *world
                    .write_storage::<Position>()
                    .get_mut(world.read_resource::<PlayerEntity>().0)
                    .expect("player has no position!") = surface_position;
                *world.write_resource::<Map>() = map;
                *world.write_resource::<CurrentMap>() = CurrentMap(0);
                deletion_dispatcher.dispatch(&world.res);
            }
            Step::GameOver => {
                println!("game over");
                let my_font = Font::builder(&renderer.context())
                    .family("Arial")
                    .size(16.0)
                    .build()
                    .unwrap();
                my_font.write(&layer, "blablabla", (10.0, 10.0), Color::RED);

                display.poll_events();
                layer.clear();
                display.clear_frame(Color::BLACK);
                renderer.draw_layer(&layer, 0);

                display.swap_frame();

                return !display.was_closed();
            }
            _ => {}
        }

        world.maintain();

        world.write_resource::<GameState>().step = Step::WaitingForInput;

        if generate_stuff {
            generate_chests(&mut world);
            generate_enemies(&mut world);
            generate_dungeon_vegetation(&mut world);
        }

        display.poll_events();

        let pressed = pressed_commands(&input);
        if pressed.len() > 0 {
            world.write_resource::<GameState>().step = Step::MovementStep;
        }
        *world.write_resource::<InputCommandSet>() = InputCommandSet(pressed);

        time_step += frame.delta_f32;
        if time_step > 100.0 {
            time_step -= 100.0;
        }
        world.write_resource::<GameState>().time_delta = time_step;

        dispatcher.dispatch(&world.res);
        world.maintain();

        if input.pressed(InputId::Escape, true) {
            return false;
        }

        layer.clear();

        let map = world.read_resource::<Map>();
        let player_entity = world.read_resource::<PlayerEntity>();
        let player_position = world
            .read_storage::<Position>()
            .get(player_entity.0)
            .expect("Player has no position!")
            .clone();

        use radiant_rs::Font;

        for pos_y in -20i32..20 {
            for pos_x in -20i32..20 {
                let pos = Position {
                    y: player_position.y + pos_y,
                    x: player_position.x + pos_x,
                };
                let y = ((pos_y + 20) * 10) as f32;
                let x = ((pos_x + 20) * 10) as f32;

                if pos.y == player_position.y && pos.x == player_position.x {
                    sprite.draw(&layer, 1, (x, y), Color::WHITE);
                    continue;
                }

                let color = if world.read_resource::<CurrentMap>().0 != 0 {
                    let color_mod = {
                        let total_diff = (pos_y.abs() + pos_x.abs()) as f32 * 1.2;
                        1.0 - (total_diff / 10.0)
                    };
                    Color(color_mod, color_mod, color_mod, 1.0)
                } else {
                    Color::WHITE
                };

                match map.tile_at(&pos) {
                    Some(TileType::Grass) => {
                        sprite.draw(&layer, 10, (x, y), color);
                    }
                    Some(TileType::Floor) => {
                        sprite.draw_transformed(&layer, 2, (x, y), color, 0.0, (1.2, 1.2));
                    }
                    Some(TileType::Tree) => {
                        sprite.draw(&layer, 14, (x, y), color);
                    }
                    Some(TileType::CavePortal(_)) => {
                        sprite.draw(&layer, 5, (x, y), color);
                    }
                    Some(TileType::SurfacePortal(_)) => {
                        sprite.draw(&layer, 4, (x, y), color);
                    }
                    Some(TileType::Water) => {
                        sprite.draw(&layer, 20, (x, y), color);
                    }
                    Some(_) => continue,
                    None => continue,
                }

                let position_lookup = world.read_resource::<PositionLookup>();
                let sprites = world.read_storage::<GameSprite>();

                if let Some(entities) = position_lookup.0.get(&pos) {
                    for entity in entities.iter() {
                        if let Some(game_sprite) = sprites.get(*entity) {
                            let index = sprite_position_from_game_sprite(game_sprite);
                            sprite.draw(&layer, index, (x, y), color);
                        }
                    }
                }
            }
        }

        display.clear_frame(Color::BLACK);
        renderer.draw_layer(&layer, 0);

        display.swap_frame();

        !display.was_closed()
    });
}

fn sprite_position_from_game_sprite(game_sprite: &GameSprite) -> u32 {
    use GameSprite::*;
    match game_sprite {
        Enemy => 88,
        Chest => 197,
        Player => 87,
    }
}

fn pressed_commands(input: &Input) -> HashSet<InputCommand> {
    let mut commands = HashSet::new();
    if input.pressed(InputId::W, true) {
        commands.insert(InputCommand::MoveUp);
    }
    if input.pressed(InputId::A, true) {
        commands.insert(InputCommand::MoveLeft);
    }
    if input.pressed(InputId::S, true) {
        commands.insert(InputCommand::MoveDown);
    }
    if input.pressed(InputId::D, true) {
        commands.insert(InputCommand::MoveRight);
    }
    if input.pressed(InputId::Space, false) {
        commands.insert(InputCommand::Attack);
    }

    commands
}
