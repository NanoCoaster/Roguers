use components::Position;
use failure::Error;
use resources::{Map, PositionLookup, TileType};
use serdeconv::from_toml_file;
use slog::Logger;
use sloggers::{Config, LoggerConfig};
use specs::storage::GenericReadStorage;
use std::path::Path;

pub fn collision_at(
    position: Position,
    map: &Map,
    position_lookup: &PositionLookup,
    collidables: &impl GenericReadStorage,
) -> bool {
    if let Some(lookup_entities) = position_lookup.0.get(&position) {
        for lookup_entity in lookup_entities.iter() {
            if let Some(_) = collidables.get(*lookup_entity) {
                return true;
            }
        }
        false
    } else {
        match map[position] {
            TileType::Inaccessible | TileType::Tree => true,
            _ => false,
        }
    }
}

pub fn try_logger_from_config() -> Result<Logger, Error> {
    let config: LoggerConfig = from_toml_file(Path::new("./logs.toml"))?;
    Ok(config.build_logger()?)
}

pub fn logger_from_config() -> Logger {
    let config: LoggerConfig = from_toml_file(Path::new("./logs.toml"))
        .unwrap_or_else(|err| panic!("Error initialising logger: {}", err));
    config
        .build_logger()
        .unwrap_or_else(|err| panic!("Error initialising logger: {}", err))
}

macro_rules! unwrap_or_return {
    ($x: expr) => {{
        match $x {
            Some(p) => p,
            None => return,
        }
    }};
}
