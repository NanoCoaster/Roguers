use std::collections::{HashMap, HashSet};

use components::Position;

use specs::Entity;
use std::default::Default;

/// Signals which systems should run
#[derive(Debug, Copy, Clone)]
pub enum Step {
    WaitingForInput,
    MovementStep,
    EnteringCave(u32, Option<Position>),
    EnteringSurface(Position),
    GameOver,
}

pub struct GameState {
    pub step: Step,
    pub time_delta: f32,
}
impl Default for GameState {
    fn default() -> Self {
        GameState {
            step: Step::WaitingForInput,
            time_delta: 0.0,
        }
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Copy, Clone)]
pub enum InputCommand {
    MoveUp,
    MoveRight,
    MoveDown,
    MoveLeft,

    LookUp,
    LookRight,
    LookDown,
    LookLeft,

    Attack,
}

use std::fmt;
// TODO: Look for a macro or something to simplify this. Write one yourself if needed.
impl fmt::Display for InputCommand {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        use self::InputCommand::*;
        let string = match self {
            MoveUp => "Move up",
            MoveRight => "Move right",
            MoveDown => "Move down",
            MoveLeft => "Move left",

            LookUp => "Look up",
            LookRight => "Look right",
            LookDown => "Look down",
            LookLeft => "Look left",

            Attack => "Attack",
        };
        write!(f, "{}", string)
    }
}

/// Commands that need to be processed in the next step
#[derive(Default)]
pub struct InputCommandSet(pub HashSet<InputCommand>);

// TODO: make this survive serialisation
#[derive(Debug)]
pub struct PlayerEntity(pub Entity);

// TODO: Evaluate the use of HashMap, maybe there's a better type for this?
#[derive(Debug, Default)]
pub struct PositionLookup(pub HashMap<Position, HashSet<Entity>>);

impl PositionLookup {
    /// Insert a new position->entity mapping
    pub fn insert(&mut self, position: Position, entity: Entity) {
        let entry = self.0.entry(position).or_insert(HashSet::new());
        entry.insert(entity);
    }

    /// Remove a position->entity mapping, deleting the position entry if no entities are mapped to it
    pub fn remove(&mut self, position: Position, entity: Entity) {
        if let Some(old) = self.0.get_mut(&position) {
            old.remove(&entity);
        } else {
            return;
        }

        if self.0[&position].is_empty() {
            self.0.remove(&position);
        }
    }

    /// Replace a position->entity mapping:
    /// *  Search the whole HashMap for `entity`; this of course assumes that each entity only has one position!
    /// *  Delete the found entry & insert a new entry with the new position
    pub fn replace(&mut self, new_position: Position, entity: Entity) {
        let pos = match self.0.iter().find(|(_, es)| es.contains(&entity)) {
            Some(p) => p.0.clone(),
            None => return,
        };
        self.remove(pos, entity);
        self.insert(new_position, entity);
    }
}

// #[derive(Debug)]
// pub struct MapInfo {
//     pub seed: [u8; 16],
// }

// /// Maps map IDs to information about the maps
// #[derive(Debug, Default)]
// pub struct MapCollection {
//     pub map_infos: HashMap<u32, MapInfo>,
//     pub current_map: u32,
// };
#[derive(Debug, Default, Copy, Clone, Serialize, Deserialize)]
pub struct CurrentMap(pub u32);

#[derive(Debug, Default, Copy, Clone, Serialize, Deserialize)]
pub struct Seed(pub [u8; 16]);

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct PortalMappings(pub HashMap<u32, Position>);
