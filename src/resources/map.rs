use rand::prelude::*;
use rand::rngs::SmallRng;
use rand::SeedableRng;
use std::ops::{Index, IndexMut};

use components::Position;
use resources::PortalMappings;

#[derive(Debug, Copy, Clone)]
pub enum TileType {
    Floor,
    Grass,
    Inaccessible,
    Tree,
    CavePortal(u32),
    SurfacePortal(Position),
    Water,
}

/// Implementation of the ingame map
#[derive(Debug, Default)]
pub struct Map {
    pub tiles: Vec<Vec<TileType>>,
    pub seed: [u8; 16],
}

impl Index<Position> for Map {
    type Output = TileType;

    fn index(&self, position: Position) -> &TileType {
        &self.tiles[position.y as usize][position.x as usize]
    }
}

impl IndexMut<Position> for Map {
    fn index_mut(&mut self, position: Position) -> &mut TileType {
        &mut self.tiles[position.y as usize][position.x as usize]
    }
}

#[derive(Debug, Eq, PartialEq)]
struct Distance {
    pub from_top: i32,
    pub from_bottom: i32,
    pub from_right: i32,
    pub from_left: i32,
}

fn distance_from_border(pos: &Position, size: i32) -> Distance {
    Distance {
        from_top: pos.y,
        from_bottom: size - 1 - pos.y,
        from_right: size - 1 - pos.x,
        from_left: pos.x,
    }
}

#[test]
fn distance_test() {
    let size = 10;
    let position = Position { y: 7, x: 1 };
    let proper_distances = Distance {
        from_top: 7,
        from_left: 1,
        from_bottom: 2,
        from_right: 8,
    };
    assert_eq!(distance_from_border(&position, size), proper_distances);
}

impl Map {
    pub fn tile_at(&self, pos: &Position) -> Option<TileType> {
        let (y, x) = (pos.y as usize, pos.x as usize);
        if self.tiles.len() > y {
            if self.tiles[0].len() > x {
                Some(self.tiles[y][x])
            } else {
                None
            }
        } else {
            None
        }
    }

    /// Generate a very simple sample map with just one room
    #[allow(dead_code)]
    pub fn sample_map() -> Map {
        use self::TileType::Floor as X;
        use self::TileType::Inaccessible as o;

        let tiles = vec![
            vec![o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o],
            vec![o, X, X, X, X, X, X, X, X, X, X, X, X, X, X, o],
            vec![o, X, X, X, X, X, X, X, X, X, X, X, X, X, X, o],
            vec![o, X, X, X, X, X, X, X, X, X, X, X, X, X, X, o],
            vec![o, X, X, X, X, X, X, X, X, X, X, X, X, X, X, o],
            vec![o, X, X, X, X, X, X, X, X, X, X, X, X, X, X, o],
            vec![o, X, X, X, X, X, X, X, X, X, X, X, X, X, X, o],
            vec![o, X, X, X, X, X, X, X, X, X, X, X, X, X, X, o],
            vec![o, X, X, X, X, X, X, X, X, X, X, X, X, X, X, o],
            vec![o, X, X, X, X, X, X, X, X, X, X, X, X, X, X, o],
            vec![o, X, X, X, X, X, X, X, X, X, X, X, X, X, X, o],
            vec![o, X, X, X, X, X, X, X, X, X, X, X, X, X, X, o],
            vec![o, X, X, X, X, X, X, X, X, X, X, X, X, X, X, o],
            vec![o, X, X, X, X, X, X, X, X, X, X, X, X, X, X, o],
            vec![o, X, X, X, X, X, X, X, X, X, X, X, X, X, X, o],
            vec![o, X, X, X, X, X, X, X, X, X, X, X, X, X, X, o],
            vec![o, o, o, o, o, o, o, o, o, o, o, o, o, o, o, o],
        ];

        Map {
            tiles,
            seed: [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        }
    }

    pub fn generate_surface(
        size: u32,
        number_of_caves: u32,
        seed: &[u8; 16],
        portal_mappings: &mut PortalMappings,
    ) -> Map {
        use fuss::Simplex;
        let simplex = Simplex::from_seed(seed.clone().into_iter().map(|x| *x as usize).collect());
        let mut tiles = vec![vec![TileType::Inaccessible; size as usize]; size as usize];
        let mut rng = SmallRng::from_seed(*seed);
        let mut caves = 0;

        // Generate forests from simplex noise
        for y in 1..(size - 1) as usize {
            for x in 1..(size - 1) as usize {
                if simplex.sum_octave_2d(5, x as f32, y as f32, 0.2, 0.06) > 0.3 {
                    tiles[y][x] = TileType::Tree;
                } else {
                    tiles[y][x] = TileType::Grass;
                }
            }
        }

        let simplex_lakes =
            Simplex::from_seed(seed.clone().into_iter().map(|x| *x as usize + 1).collect());
        for y in 1..(size - 1) as usize {
            for x in 1..(size - 1) as usize {
                if simplex_lakes.sum_octave_2d(5, x as f32, y as f32, 0.2, 0.03) > 0.5 {
                    tiles[y][x] = TileType::Water;
                }
            }
        }

        // Make borders inaccessible
        for x in 0..size as usize {
            tiles[0][x] = TileType::Inaccessible;
            tiles[x][0] = TileType::Inaccessible;
        }

        // Generate cave portals
        'outer: loop {
            for y in 1..(size - 1) as usize {
                for x in 1..(size - 1) as usize {
                    if rng.gen_range(0, (size * size) / number_of_caves) == 1 {
                        tiles[y][x] = TileType::CavePortal(caves + 1);
                        portal_mappings.0.insert(
                            caves + 1,
                            Position {
                                y: y as i32,
                                x: x as i32,
                            },
                        );
                        caves += 1;
                        if caves >= number_of_caves {
                            break 'outer;
                        }
                    }
                }
            }
        }

        Map { seed: *seed, tiles }
    }

    /// Generate a random map using a modified Drunkard Walk algorithm
    ///
    /// The drunkard walk is a very simple algorithm for creating cave-like maps:
    ///   1. Mark the whole map as inaccessible
    ///   2. Choose a random starting point and mark it as accessible
    ///   3. Choose a random direction and make one step in it
    ///   4. Mark the new position as accessible; increase count if it wasn't already accessible
    ///   5. Repeat steps 3 & 4 until the chosen `size` is reached
    ///
    /// It is used here with a slight variation in the form of biases.
    /// There are slight biases introduced to keep going in one direction (to create corridor-like structures),
    /// as well as to keep away from the edges, as it tends to look very unnatural otherwise.
    fn generate_tiles_drunkard(
        size: i32,
        empty_portion: usize,
        seed: &[u8; 16],
        seed_offset: u8,
        parent_map_position: Position,
    ) -> Vec<Vec<TileType>> {
        let mut seed = seed.clone();
        seed[1] += seed_offset;
        let mut rng = SmallRng::from_seed(seed);

        let mut tiles = vec![vec![TileType::Inaccessible; size as usize]; size as usize];

        let mut emptied = 0;
        let mut pos = Position {
            y: size / 2,
            x: size / 2,
        };

        let mut last_direction = 0;
        let mut last_direction_modifier = 1;

        let mut highest_y = Position { y: pos.y, x: pos.x };
        let (mut lowest_y, mut highest_x, mut lowest_x) = (highest_y, highest_y, highest_y);
        let cutoff = 10;

        // Main algorithm loop
        while emptied < empty_portion {
            let Distance {
                from_bottom,
                from_left,
                from_top,
                from_right,
            } = distance_from_border(&pos, (size - 1) as i32);

            // Apply cutoffs so that the cave isn't just bunched together in the middle
            let mut prob_top = if from_top > cutoff {
                cutoff
            } else {
                from_top - 1
            };
            let mut prob_bottom = if from_bottom > cutoff {
                cutoff
            } else {
                from_bottom
            };
            let mut prob_right = if from_right > cutoff {
                cutoff
            } else {
                from_right
            };
            let mut prob_left = if from_left > cutoff {
                cutoff
            } else {
                from_left - 1
            };

            // Apply bias so that corridors are generated: Walking in one direction multiple times is encouraged
            if rng.gen_range(0, last_direction_modifier) > 0 {
                match last_direction {
                    0 => if from_bottom < (size - 2) {
                        prob_bottom += 5;
                    },
                    1 => if from_left > 1 {
                        prob_left += 5;
                    },
                    2 => if from_right < (size - 2) {
                        prob_right += 5;
                    },
                    3 => if from_top > 1 {
                        prob_top += 5;
                    },
                    _ => (),
                }
            }

            // Cut off at level size
            let correct_for_size = |num| {
                if num >= size {
                    size - 1
                } else {
                    num
                }
            };
            prob_top = correct_for_size(prob_top);
            prob_bottom = correct_for_size(prob_bottom);
            prob_right = correct_for_size(prob_right);
            prob_left = correct_for_size(prob_left);

            // Cut off again (done more than once for better looking results)
            prob_top = if prob_top > cutoff { cutoff } else { prob_top };
            prob_bottom = if prob_bottom > cutoff {
                cutoff
            } else {
                prob_bottom
            };
            prob_right = if prob_right > cutoff {
                cutoff
            } else {
                prob_right
            };
            prob_left = if prob_left > cutoff {
                cutoff
            } else {
                prob_left
            };

            let propability_sum = prob_top + prob_bottom + prob_right + prob_left;

            // Set the last direction modifier: corridors up to a length of 5 get an increasing chance
            //  of being continued.
            let mut account_for_last_direction = |dir| {
                if last_direction == dir {
                    if (last_direction_modifier + 1) % 5 == 0 {
                        last_direction_modifier = 1;
                    } else {
                        last_direction_modifier += 1;
                    }
                } else {
                    last_direction_modifier = 1;
                }
                last_direction = dir;
            };

            let p = rng.gen_range(1, propability_sum);
            match p {
                p if p < prob_bottom => {
                    // bottom
                    pos.y += 1;
                    account_for_last_direction(0);
                    if pos.y > highest_y.y {
                        highest_y = pos;
                    }
                }
                p if p >= prob_bottom && p < (prob_left + prob_bottom) => {
                    // left
                    pos.x -= 1;
                    account_for_last_direction(1);
                    if pos.x < lowest_x.x {
                        lowest_x = pos;
                    }
                }
                p if p >= (prob_left + prob_bottom)
                    && p < (prob_left + prob_bottom + prob_right) =>
                {
                    // right
                    pos.x += 1;
                    account_for_last_direction(2);
                    if pos.x > highest_x.x {
                        highest_x = pos;
                    }
                }
                // TODO: Why the need for -1 here? If I leave it out, there's a notable bias towards the top, wth
                // Fuck off-by-one errors
                p if p >= (prob_left + prob_bottom + prob_right) && p < propability_sum - 1 => {
                    // top
                    pos.y -= 1;
                    account_for_last_direction(3);
                    if pos.y < lowest_y.y {
                        lowest_y = pos;
                    }
                }
                _ => (),
            };

            match tiles[pos.y as usize][pos.x as usize] {
                // Already emptied, do nothing
                TileType::Floor => (),
                // Inaccessible, mark as accessible and increase count
                _ => {
                    tiles[pos.y as usize][pos.x as usize] = TileType::Floor;
                    emptied += 1;
                }
            }
        }

        println!(
            "Highest y: {:?}  Highest x: {:?}  Lowest y: {:?}  Lowest x: {:?}",
            highest_y, highest_x, lowest_y, lowest_x
        );

        // Place surface portal in the middle
        tiles[(size / 2) as usize][(size / 2) as usize] =
            TileType::SurfacePortal(parent_map_position);
        tiles
    }

    /// Generate a random cave
    ///
    /// * `size` - Size of the map, quadratic maps only for now
    /// * `empty_portion` - How big the cave should be (in tiles)
    pub fn generate_cave(
        size: u32,
        empty_portion: usize,
        seed: &[u8; 16],
        seed_offset: u8,
        parent_map_position: Position,
    ) -> Map {
        let tiles = Map::generate_tiles_drunkard(
            size as i32,
            empty_portion,
            seed,
            seed_offset,
            parent_map_position,
        );

        Map { tiles, seed: *seed }
    }

    /// Print the map to stdout - just for debugging purposes
    pub fn print(&self) {
        use TileType::*;
        let mut string = "Map:\n".to_string();
        for y in &self.tiles {
            for x in y {
                string.push_str(match *x {
                    Water => "~",
                    Grass => ",",
                    Floor => ".",
                    Inaccessible => "#",
                    Tree => "t",
                    SurfacePortal(_) => "p",
                    CavePortal(_) => "p",
                });
            }
            string.push('\n');
        }

        println!("{}", string);
    }
}
