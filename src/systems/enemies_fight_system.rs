use components::*;
use resources::*;
use specs::prelude::*;

use slog::Logger;

pub struct EnemiesFightSystem {
    pub logger: Logger,
    pub delta: f32,
}
impl std::default::Default for EnemiesFightSystem {
    fn default() -> Self {
        EnemiesFightSystem {
            logger: ::utils::logger_from_config(),
            delta: 0.0,
        }
    }
}

impl<'a> System<'a> for EnemiesFightSystem {
    type SystemData = (
        WriteStorage<'a, Health>,
        ReadStorage<'a, CombatStats>,
        Write<'a, GameState>,
        WriteStorage<'a, Position>,
        ReadStorage<'a, EnemyMarker>,
        Option<Read<'a, PlayerEntity>>,
        WriteStorage<'a, NpcActivities>,
        Entities<'a>,
    );

    fn run(
        &mut self,
        (
            mut healths,
            combat_stats,
            mut game_state,
            mut positions,
            enemy_markers,
            player_entity,
            mut npc_activites,
            entities,
        ): Self::SystemData,
    ) {
        if game_state.time_delta - self.delta < 0.5 {
            return;
        }
        self.delta = game_state.time_delta;

        // Only continue if there's a valid player active in the game
        let player = match player_entity {
            Some(p) => p.0,
            None => {
                debug!(self.logger, "No active player entity!");
                return;
            }
        };

        let player_position = positions
            .get(player)
            .expect("Player entity has no position!")
            .clone();

        for (pos, activities, stats, _, _) in (
            &mut positions,
            &mut npc_activites,
            &combat_stats,
            &*entities,
            &enemy_markers,
        )
            .join()
        {
            // Attack if there's a player around
            if pos.x + 1 == player_position.x && pos.y == player_position.y
                || pos.x - 1 == player_position.x && pos.y == player_position.y
                || pos.y + 1 == player_position.x && pos.x == player_position.x
                || pos.y - 1 == player_position.x && pos.x == player_position.x
            {
                let player_health = healths.get_mut(player).expect("player has no health!");
                let player_stats = combat_stats
                    .get(player)
                    .expect("player has no combat stats!");
                player_health.0 -= stats.attack - player_stats.defense;

                // Keep attacking if the player isn't dead yet
                if player_health.0 > 0.0 {
                    match activities.0.last() {
                        Some(NpcState::Attacking) => (),
                        _ => activities.0.push(NpcState::Attacking),
                    }
                } else {
                    game_state.step = Step::GameOver;
                }
            } else if let Some(NpcState::Attacking) = activities.0.last() {
                // Stop attacking if the player has left the area, resume doing whatever
                activities.0.pop();
            }
        }
    }
}
