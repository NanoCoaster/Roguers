use std::fs;

use components::*;
use resources::*;

use slog::Logger;
use specs::error::NoError;
use specs::prelude::Read as SpecsRead;
use specs::prelude::*;
use specs::saveload::*;

use ron::ser::{to_string, Serializer};

pub struct Serialize {
    logger: Logger,
}
impl std::default::Default for Serialize {
    fn default() -> Self {
        Serialize {
            logger: ::utils::logger_from_config(),
        }
    }
}

impl<'a> System<'a> for Serialize {
    type SystemData = (
        Entities<'a>,
        ReadStorage<'a, Position>,
        ReadStorage<'a, Health>,
        ReadStorage<'a, CollidableMarker>,
        ReadStorage<'a, LookDirection>,
        ReadStorage<'a, CombatStats>,
        ReadStorage<'a, Score>,
        ReadStorage<'a, Loot>,
        ReadStorage<'a, GameSprite>,
        ReadStorage<'a, EnemyMarker>,
        ReadStorage<'a, NpcVelocity>,
        SpecsRead<'a, CurrentMap>,
        SpecsRead<'a, Seed>,
        ReadStorage<'a, U64Marker>,
    );
    fn run(
        &mut self,
        (
            ents,
            pos,
            healths,
            collidables,
            look_directions,
            combat_stats,
            scores,
            loots,
            game_sprites,
            enemy_markers,
            npc_velocities,
            current_map,
            seed,
            markers,
        ): Self::SystemData,
    ) {
        info!(self.logger, "Saving game...");
        let mut ser = Serializer::new(Some(Default::default()), true);
        SerializeComponents::<NoError, U64Marker>::serialize(
            &(
                &pos,
                &healths,
                &collidables,
                &look_directions,
                &combat_stats,
                &scores,
                &loots,
                &game_sprites,
                &enemy_markers,
                &npc_velocities,
            ),
            &ents,
            &markers,
            &mut ser,
        ).unwrap_or_else(|e| error!(self.logger, "Error serializing save data: {}", e));

        let mut string = String::new();
        string.push_str(&to_string(&seed.0).expect("error serialising seed"));
        string.push_str("\n");
        string.push_str(&to_string(&current_map.0).expect("error serialising current map ID"));
        string.push_str("\n");
        string.push_str(&ser.into_output_string());

        if let Err(e) = fs::write("savegame.ron", &string) {
            error!(self.logger, "Error writing savegame to file: {}", e);
        }

        println!("{}", string);
    }
}
