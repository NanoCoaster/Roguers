use amethyst::ecs::{Entities, Join, LazyUpdate, Read, ReadStorage, System, WriteStorage};

use components::{CombatStats, FightToken, Health, Loot, Score};

use slog::Logger;
/// Handles fights (attacks) between entities
///
/// This is the second part of an attack: First, a FightToken has to be set, which this system then uses to process the attack.
pub struct FightSystem {
    logger: Logger,
}
impl std::default::Default for FightSystem {
    fn default() -> Self {
        FightSystem { logger: ::utils::logger_from_config() }
    }
}

impl<'a> System<'a> for FightSystem {
    type SystemData = (
        WriteStorage<'a, Health>,
        ReadStorage<'a, FightToken>,
        ReadStorage<'a, CombatStats>,
        ReadStorage<'a, Loot>,
        WriteStorage<'a, Score>,
        Read<'a, LazyUpdate>,
        Entities<'a>,
    );

    fn run(
        &mut self,
        (mut healths, fight_tokens, combat_stats, loots, mut scores, update, entities): <Self as System>::SystemData,
){
        for (hp, def_stats, token, entity) in
            (&mut healths, &combat_stats, &fight_tokens, &*entities).join()
        {
            let atk_stats = combat_stats.get(token.attacker).unwrap();
            hp.0 -= atk_stats.attack - def_stats.defense;

            println!("attacking:  {:?}", entity);

            if hp.0 <= 0.0 {
                println!("Kill!  {:?}", entity);
                if let Some(score) = scores.get_mut(token.attacker) {
                    if let Some(loot) = loots.get(entity) {
                        score.0 += loot.0;
                        println!("Score:  +{} -> {}", loot.0, score.0);
                    }
                }

                if let Err(e) = entities.delete(entity) {
                    panic!("ERROR:    {}", e);
                }
            } else {
                println!("attack on: {:?} - HP afterwards: {}", entity, hp.0);
            }

            update.remove::<FightToken>(entity);
        }
    }
}
