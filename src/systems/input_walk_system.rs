use components::*;
use resources::*;
use specs::prelude::*;

use slog::Logger;

use utils::*;

/// Manages movement by the player
pub struct InputWalkSystem {
    logger: Logger,
}
impl std::default::Default for InputWalkSystem {
    fn default() -> Self {
        InputWalkSystem {
            logger: ::utils::logger_from_config(),
        }
    }
}

impl<'a> System<'a> for InputWalkSystem {
    type SystemData = (
        Write<'a, GameState>,
        Read<'a, InputCommandSet>,
        WriteStorage<'a, Position>,
        Read<'a, PositionLookup>,
        ReadStorage<'a, CollidableMarker>,
        Read<'a, Map>,
        Option<Read<'a, PlayerEntity>>,
        Read<'a, CurrentMap>,
        Read<'a, PortalMappings>,
    );

    fn run(
        &mut self,
        (
            mut game_state,
            commands,
            mut positions,
            position_lookup,
            collidables,
            map,
            player_entity,
            current_map,
            portal_mappings,
        ): Self::SystemData,
    ) {
        match game_state.step {
            Step::MovementStep => (),
            _ => return,
        }

        // Only continue if the player is currently active in the game
        let player = unwrap_or_return!(player_entity).0;

        let mut new_pos = positions
            .get(player)
            .expect("player doesn't have position component!")
            .clone();

        if commands.0.contains(&InputCommand::MoveUp) {
            new_pos.y -= 1;
        } else if commands.0.contains(&InputCommand::MoveDown) {
            new_pos.y += 1;
        } else if commands.0.contains(&InputCommand::MoveLeft) {
            new_pos.x -= 1;
        } else if commands.0.contains(&InputCommand::MoveRight) {
            new_pos.x += 1;
        } else {
            return;
        }

        match map[new_pos] {
            TileType::CavePortal(x) => game_state.step = Step::EnteringCave(x, None),
            TileType::SurfacePortal(_) => {
                let surface_position = portal_mappings
                    .0
                    .get(&current_map.0)
                    .expect(&format!("no portal mapping for map id: {}", current_map.0));

                game_state.step = Step::EnteringSurface(*surface_position);
            }

            _ => {
                if !collision_at(new_pos, &map, &position_lookup, &collidables) {
                    debug!(self.logger, "Player move:  {:?}", new_pos);
                    *positions.get_mut(player).unwrap() = new_pos;
                } else {
                    debug!(self.logger, "Error on player move:  {:?}", new_pos);
                }
            }
        }
    }
}
