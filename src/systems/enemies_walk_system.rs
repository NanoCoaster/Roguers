use components::*;
use resources::*;
use specs::prelude::*;

use rand::rngs::SmallRng;
use rand::Rng;
use slog::Logger;

use rand::FromEntropy;
use utils::logger_from_config;

/// Enemies walking around randomly
pub struct EnemiesWalkSystem {
    pub logger: Logger,
    pub rng: SmallRng,
    pub counter: u32,
}
impl std::default::Default for EnemiesWalkSystem {
    fn default() -> Self {
        EnemiesWalkSystem {
            logger: logger_from_config(),
            rng: SmallRng::from_entropy(),
            counter: 0,
        }
    }
}

impl<'a> System<'a> for EnemiesWalkSystem {
    type SystemData = (
        Read<'a, Map>,
        WriteStorage<'a, LookDirection>,
        WriteStorage<'a, Position>,
        ReadStorage<'a, EnemyMarker>,
        ReadStorage<'a, NpcActivities>,
        ReadStorage<'a, NpcVelocity>,
    );

    fn run(
        &mut self,
        (
            map,
            mut look_directions,
            mut positions,
            enemy_markers,
            npc_states,
            npc_velocities,
        ): Self::SystemData,
){
        for (pos, look_dir, state, velocity, _) in (
            &mut positions,
            &mut look_directions,
            &npc_states,
            &npc_velocities,
            &enemy_markers,
        )
            .join()
        {
            if let Some(NpcState::Roaming) = state.0.last() {
                if self.counter % velocity.0 != 0 {
                    continue;
                }
                // Random walking direction
                let (new_pos, dir) = match self.rng.gen_range(0, 4) {
                    0 => (
                        Position {
                            y: pos.y - 1,
                            x: pos.x,
                        },
                        LookDirection::North,
                    ),
                    1 => (
                        Position {
                            y: pos.y,
                            x: pos.x + 1,
                        },
                        LookDirection::East,
                    ),
                    2 => (
                        Position {
                            y: pos.y + 1,
                            x: pos.x,
                        },
                        LookDirection::South,
                    ),
                    3 => (
                        Position {
                            y: pos.y,
                            x: pos.x - 1,
                        },
                        LookDirection::West,
                    ),
                    _ => panic!("RNG number out of range! {EnemiesWalkSystem}"),
                };

                // Only walk if it's an accessible tile
                if let TileType::Floor = map[new_pos] {
                    *pos = new_pos;
                    *look_dir = dir;
                }
            }
        }

        // Cut off velocity tick counter at 1000
        self.counter += 1;
        if self.counter > 1000 {
            self.counter = 1;
        }
    }
}
