use components::*;
use resources::*;
use specs::prelude::*;

use slog::Logger;

/// Manages movement by the player
pub struct InputLookDirectionSystem {
    logger: Logger,
}
impl std::default::Default for InputLookDirectionSystem {
    fn default() -> Self {
        InputLookDirectionSystem {
            logger: ::utils::logger_from_config(),
        }
    }
}

impl<'a> System<'a> for InputLookDirectionSystem {
    type SystemData = (
        Read<'a, GameState>,
        Read<'a, InputCommandSet>,
        WriteStorage<'a, LookDirection>,
        Option<Read<'a, PlayerEntity>>,
    );

    fn run(
        &mut self,
        (game_state, commands, mut look_directions, player_entity): Self::SystemData,
    ) {
        match game_state.step {
            Step::MovementStep => (),
            _ => return,
        }
        // Only continue if the player is currently active in the game
        let player = unwrap_or_return!(player_entity).0;

        let dir = look_directions
            .get_mut(player)
            .expect("player doesn't have look direction component!");

        if commands.0.contains(&InputCommand::MoveUp) || commands.0.contains(&InputCommand::LookUp)
        {
            *dir = LookDirection::North;
        } else if commands.0.contains(&InputCommand::MoveDown)
            || commands.0.contains(&InputCommand::LookDown)
        {
            *dir = LookDirection::South;
        } else if commands.0.contains(&InputCommand::MoveLeft)
            || commands.0.contains(&InputCommand::LookLeft)
        {
            *dir = LookDirection::West;
        } else if commands.0.contains(&InputCommand::MoveRight)
            || commands.0.contains(&InputCommand::LookRight)
        {
            *dir = LookDirection::East;
        } else {
            return;
        }

        debug!(self.logger, "Player looking  {:?}", *dir);
    }
}
