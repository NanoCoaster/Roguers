//use amethyst::ecs::{Entities, LazyUpdate, Read, System, Write, ReadStorage};
//use components::*;
//use fuss::Simplex;
//use rayon::prelude::*;
//use resources::{Map, PositionLookup, TileType};
//use systems::utils::*;
//use slog::Logger;
//
///// Generates random trees (vegetation with collision)
//pub struct GenerateVegetationSystem {
//    logger: Logger,
//}
//impl std::default::Default for GenerateVegetationSystem {
//    fn default() -> Self {
//        GenerateVegetationSystem { logger: ::utils::logger_from_config() }
//    }
//}
//
//impl<'a> System<'a> for GenerateVegetationSystem {
//    type SystemData = (
//        Write<'a, Map>,
//        Read<'a, PositionLookup>,
//        ReadStorage<'a, CollidableMarker>,
//    );
//
//    fn run(&mut self, (mut map, position_lookup, collidables): Self::SystemData) {
//        let positions = generate_simplex(&*map, 0, 0.6, &|_, pos| {
//            !collision_at(pos, &map, &position_lookup, &collidables)
//        });
//
//        for pos in positions.into_iter() {
//            map[pos] = TileType::Tree;
//            println!("tree at:  {:?}", pos);
//        }
//    }
//}
//
///// Generates random chests
//pub struct GenerateChestsSystem {
//    logger: Logger,
//}
//impl std::default::Default for GenerateChestsSystem {
//    fn default() -> Self {
//        GenerateChestsSystem { logger: ::utils::logger_from_config() }
//    }
//}
//
//impl<'a> System<'a> for GenerateChestsSystem {
//    type SystemData = (
//        Read<'a, Map>,
//        Read<'a, PositionLookup>,
//        ReadStorage<'a, CollidableMarker>,
//        Read<'a, LazyUpdate>,
//        Entities<'a>,
//    );
//
//    fn run(&mut self, (map, position_lookup, collidables, update, entities): Self::SystemData) {
//        let positions = generate_simplex(&*map, 1, 0.6, &|_, pos| {
//            !collision_at(pos, &map, &position_lookup, &collidables)
//        });
//
//        for pos in positions.into_iter() {
//            println!("chest at:  {:?}", pos);
//            update
//                .create_entity(&entities)
//                .with(DisplayName("Chest".into()))
//                .with(pos)
//                .with(Health(2.0))
//                .with(CombatStats {
//                    attack: 0.0,
//                    defense: 0.0,
//                })
//                .with(Loot(30))
//                .build();
//        }
//    }
//}
//
///// Generates random enemies
//pub struct GenerateEnemiesSystem {
//    logger: Logger,
//}
//impl std::default::Default for GenerateEnemiesSystem {
//    fn default() -> Self {
//        GenerateEnemiesSystem { logger: ::utils::logger_from_config() }
//    }
//}
//
//impl<'a> System<'a> for GenerateEnemiesSystem {
//    type SystemData = (
//        Read<'a, Map>,
//        Read<'a, PositionLookup>,
//        ReadStorage<'a, CollidableMarker>,
//        Read<'a, LazyUpdate>,
//        Entities<'a>,
//    );
//
//    fn run(&mut self, (map, position_lookup, collidables, update, entities): Self::SystemData) {
//        let positions = generate_simplex(&*map, 2, 0.6, &|_, pos| {
//            !collision_at(pos, &map, &position_lookup, &collidables)
//        });
//
//        for pos in positions.into_iter() {
//            println!("enemy at:  {:?}", pos);
//            update
//                .create_entity(&entities)
//                .with(DisplayName("Enemy".into()))
//                .with(pos)
//                .with(Health(20.0))
//                .with(CombatStats {
//                    attack: 3.0,
//                    defense: 2.0,
//                })
//                .with(CollidableMarker)
//                .build();
//        }
//    }
//}
//
///// Generate a list of positions based on simplex noise and the map
/////
///// * `map` - The `Map`, also containing the seed used
///// * `seed_offset` - This is added (wrapping) to the seed; used for variation between different generators using the same basic seed
///// * `simplex_cutoff` - Value between `0.0` and `1.0`, if noise at y,x is bigger than this value, the position is used
///// * `predicate` - custom predicate that can be used for further validation; needs to be `Send+Sync` because rayon parallel iterators are used in the iteration
//fn generate_simplex<'a, F>(
//    map: &Map,
//    seed_offset: usize,
//    simplex_cutoff: f32,
//    predicate: F,
//) -> Vec<Position>
//where
//    F: Send + Sync + Fn(TileType, Position) -> bool,
//{
//    let mut seed: Vec<usize> = map.seed.clone().into_iter().map(|x| *x as usize).collect();
//    seed[0] = seed[0].wrapping_add(seed_offset);
//    let simplex = Simplex::from_seed(seed);
//
//    map.tiles
//        .par_iter()
//        .enumerate()
//        .flat_map(|(y, row)| {
//            row.iter()
//                .enumerate()
//                .filter_map(|(x, tile)| {
//                    let pos = Position {
//                        y: y as i32,
//                        x: x as i32,
//                    };
//                    if predicate(*tile, pos) {
//                        if simplex.noise_2d(y as f32, x as f32) > simplex_cutoff {
//                            Some(pos)
//                        } else {
//                            None
//                        }
//                    } else {
//                        None
//                    }
//                })
//                .collect::<Vec<Position>>()
//        })
//        .collect()
//}
