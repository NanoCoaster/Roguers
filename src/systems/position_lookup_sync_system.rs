use specs::prelude::*;

use components::Position;
use resources::*;
use slog::Logger;

// https://docs.rs/specs/0.11.2/specs/struct.FlaggedStorage.html
// TODO: Maybe find a way to get those `Option`s out of here?
#[derive(Debug)]
pub struct PositionLookupSyncSystem {
    logger: Logger,
    pub inserted: BitSet,
    pub removed: BitSet,
    pub modified: BitSet,
    pub insert_reader_id: Option<ReaderId<InsertedFlag>>,
    pub remove_reader_id: Option<ReaderId<RemovedFlag>>,
    pub modified_reader_id: Option<ReaderId<ModifiedFlag>>,
}
impl std::default::Default for PositionLookupSyncSystem {
    fn default() -> Self {
        PositionLookupSyncSystem {
            logger: ::utils::logger_from_config(),
            inserted: std::default::Default::default(),
            removed: std::default::Default::default(),
            modified: std::default::Default::default(),
            insert_reader_id: std::default::Default::default(),
            remove_reader_id: std::default::Default::default(),
            modified_reader_id: std::default::Default::default(),
        }
    }
}

impl<'a> System<'a> for PositionLookupSyncSystem {
    type SystemData = (
        Write<'a, PositionLookup>,
        ReadStorage<'a, Position>,
        Entities<'a>,
    );

    fn setup(&mut self, res: &mut Resources) {
        Self::SystemData::setup(res);
        let mut positions: WriteStorage<Position> = SystemData::fetch(&res);
        self.insert_reader_id = Some(positions.track_inserted());
        self.remove_reader_id = Some(positions.track_removed());
        self.modified_reader_id = Some(positions.track_modified());
    }

    fn run(&mut self, (mut lookup, positions, entities): Self::SystemData) {
        self.inserted.clear();
        self.modified.clear();
        self.removed.clear();
        let mut inserted = self
            .insert_reader_id
            .as_mut()
            .expect("Position lookup sync system error");
        let mut removed = self
            .remove_reader_id
            .as_mut()
            .expect("Position lookup sync system error");
        let mut modified = self
            .modified_reader_id
            .as_mut()
            .expect("Position lookup sync system error");

        positions.populate_inserted(&mut inserted, &mut self.inserted);
        positions.populate_removed(&mut removed, &mut self.removed);
        positions.populate_modified(&mut modified, &mut self.modified);

        // TODO: parallelize this
        // FIXME: https://slide-rs.github.io/specs/12_tracked.html "It is because of this scenario you should always verify that the Component still exist in the storage, and that the Entity is still alive."
        for (position, entity, _) in (&positions, &*entities, &self.removed).join() {
            lookup.remove(*position, entity);
        }

        for (position, entity, _) in (&positions, &*entities, &self.inserted).join() {
            lookup.insert(*position, entity);
        }

        for (position, entity, _) in (&positions, &*entities, &self.modified).join() {
            lookup.replace(*position, entity);
        }
    }
}
