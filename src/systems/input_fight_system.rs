use components::*;
use resources::*;
use specs::prelude::*;

use slog::Logger;

/// Handles attacks by the player
pub struct InputFightSystem {
    logger: Logger,
}
impl std::default::Default for InputFightSystem {
    fn default() -> Self {
        InputFightSystem {
            logger: ::utils::logger_from_config(),
        }
    }
}

impl<'a> System<'a> for InputFightSystem {
    type SystemData = (
        Read<'a, GameState>,
        Write<'a, InputCommandSet>,
        ReadStorage<'a, Position>,
        ReadStorage<'a, LookDirection>,
        Option<Read<'a, PlayerEntity>>,
        ReadStorage<'a, CombatStats>,
        Read<'a, PositionLookup>,
        WriteStorage<'a, Health>,
        WriteStorage<'a, Score>,
        WriteStorage<'a, Loot>,
        Entities<'a>,
    );

    fn run(
        &mut self,
        (
            game_state,
            commands,
            positions,
            look_directions,
            player_entity,
            combat_stats,
            position_lookup,
            mut healths,
            mut scores,
            loots,
            entities,
        ): Self::SystemData,
    ) {
        match game_state.step {
            Step::MovementStep => {
                if !commands.0.contains(&InputCommand::Attack) {
                    return;
                }
                ()
            }
            _ => return,
        }

        // Only continue if there's a valid player active in the game
        let player = unwrap_or_return!(player_entity).0;

        let pos = positions
            .get(player)
            .expect("player doesn't have position component!");
        let dir = look_directions
            .get(player)
            .expect("player doesn't have look direction component!");

        let enemy_position = match dir {
            LookDirection::North => Position {
                y: pos.y - 1,
                x: pos.x,
            },
            LookDirection::South => Position {
                y: pos.y + 1,
                x: pos.x,
            },
            LookDirection::East => Position {
                y: pos.y,
                x: pos.x + 1,
            },
            LookDirection::West => Position {
                y: pos.y,
                x: pos.x - 1,
            },
        };
        // Get enemies (entities with combat_stats component)
        if let Some(other_entities) = &position_lookup.0.get(&enemy_position) {
            for other_entity in other_entities.iter() {
                if let Some(stats) = combat_stats.get(*other_entity) {
                    let atk = combat_stats
                        .get(player)
                        .expect("player has no combat stats!")
                        .attack;
                    let damage = atk - stats.defense;

                    if let Some(health) = healths.get_mut(*other_entity) {
                        health.0 -= damage;

                        if health.0 <= 0.0 {
                            debug!(self.logger, "{:?}  kills  {:?}!", player, other_entity);
                            if let Some(loot) = loots.get(*other_entity) {
                                let mut score = scores
                                    .get_mut(player)
                                    .expect("player doesn't have a score!");
                                score.0 += loot.0;
                                debug!(
                                    self.logger,
                                    "{:?}'s score:  +{} -> {}", player, loot.0, score.0
                                );
                            }

                            if let Err(e) = entities.delete(*other_entity) {
                                panic!("ERROR:    {}", e);
                            }
                        } else {
                            debug!(
                                self.logger,
                                "{:?}  attacking  {:?} - HP afterwards: {}",
                                player,
                                other_entity,
                                health.0
                            );
                        }
                    }
                }
            }
        }
    }
}
