use amethyst::{
    ecs::{Read, System, Write},
    input::{Button, InputHandler},
};
use resources::{GameSystemState, InputCommand, InputCommandSet};
use std::collections::HashSet;
use slog::Logger;

fn command_from_action(btn: Button) -> Option<InputCommand> {
    use amethyst::renderer::VirtualKeyCode as K;
    use resources::InputCommand::*;

    if let Button::Key(code) = btn {
        match code {
            K::W => Some(MoveUp),
            K::D => Some(MoveRight),
            K::S => Some(MoveDown),
            K::A => Some(MoveLeft),
            K::Space => Some(Attack),
            _ => None,
        }
    } else {
        None
    }
}

/// Checks for player input and updates the corresponding Resources
///
/// For now, just ignores keys that stay pressed (so, only key-down is propagated).
#[derive(Debug)]
pub struct InputCheckSystem {
    logger: Logger,
    pressed: HashSet<InputCommand>,
}
impl std::default::Default for InputCheckSystem {
    fn default() -> Self {
        InputCheckSystem { logger: ::utils::logger_from_config(), pressed: std::default::Default() }
    }
}

impl<'a> System<'a> for InputCheckSystem {
    type SystemData = (
        Read<'a, InputHandler<String, String>>,
        Write<'a, GameSystemState>,
        Write<'a, InputCommandSet>,
    );

    fn run(&mut self, (input, mut system_state, mut command_set): Self::SystemData) {
        let commands: Vec<InputCommand> = input
            .buttons_that_are_down()
            .filter_map(command_from_action)
            .collect();

        // Commands that weren't already pressed
        command_set.0 = commands
            .iter()
            .cloned()
            .filter(|c| !self.pressed.contains(c))
            .collect();

        // Make the game simulate one step
        if !command_set.0.is_empty() {
            system_state.0 = true;
        }

        // Save the currently pressed keys for comparison the next time
        self.pressed = commands.iter().cloned().collect();
    }
}
