use components::*;
use resources::*;
use slog::Logger;
use specs::prelude::*;

/// Mostly a hack, this deletes all entities with positions; used when chaining maps
/// Should be made obsolete once I use save/load capabilities
pub struct DeletionSystem {
    logger: Logger,
}
impl std::default::Default for DeletionSystem {
    fn default() -> Self {
        DeletionSystem {
            logger: ::utils::logger_from_config(),
        }
    }
}

impl<'a> System<'a> for DeletionSystem {
    type SystemData = (
        ReadStorage<'a, Position>,
        Entities<'a>,
        Option<Read<'a, PlayerEntity>>,
    );

    fn run(&mut self, (positions, entities, player_entity): Self::SystemData) {
        let player = unwrap_or_return!(player_entity).0;

        for (entity, _) in (&*entities, &positions).join() {
            if entity != player {
                if let Err(e) = entities.delete(entity) {
                    error!(self.logger, "Error in deletion system: {}", e);
                }
            }
        }
    }
}
