use components::*;
use resources::*;

use specs::error::NoError;
use specs::prelude::*;
use specs::saveload::*;

use ron::de::Deserializer;
use std::fs::File;
use std::io::Read;

use slog::Logger;

pub struct Deserialize {
    logger: Logger,
}
impl std::default::Default for Deserialize {
    fn default() -> Self {
        Deserialize {
            logger: ::utils::logger_from_config(),
        }
    }
}

impl<'a> System<'a> for Deserialize {
    type SystemData = (
        Entities<'a>,
        Write<'a, U64MarkerAllocator>,
        WriteStorage<'a, Position>,
        WriteStorage<'a, Health>,
        WriteStorage<'a, CollidableMarker>,
        WriteStorage<'a, LookDirection>,
        WriteStorage<'a, CombatStats>,
        WriteStorage<'a, Score>,
        WriteStorage<'a, Loot>,
        WriteStorage<'a, GameSprite>,
        WriteStorage<'a, EnemyMarker>,
        WriteStorage<'a, NpcVelocity>,
        Write<'a, CurrentMap>,
        Write<'a, Seed>,
        Write<'a, GameState>,
        WriteStorage<'a, U64Marker>,
    );

    fn run(
        &mut self,
        (
            ent,
            mut alloc,
            pos,
            healths,
            collidables,
            look_directions,
            combat_stats,
            scores,
            loots,
            game_sprites,
            enemy_markers,
            npc_velocities,
            mut current_map,
            mut seed,
            mut game_state,
            mut markers,
        ): Self::SystemData,
    ) {
        info!(self.logger, "Loading savegame...");
        let mut file = match File::open("savegame.ron") {
            Ok(x) => x,
            Err(e) => {
                error!(self.logger, "Error opening savegame file: {}", e);
                return;
            }
        };
        let mut buffer = String::new();
        if let Err(e) = file.read_to_string(&mut buffer) {
            error!(self.logger, "Error reading savegame file: {}", e);
        }

        let lines: Vec<&str> = buffer.splitn(3, '\n').collect();

        if let Ok(mut de) = Deserializer::from_str(&lines[2]) {
            DeserializeComponents::<NoError, _>::deserialize(
                &mut (
                    pos,
                    healths,
                    collidables,
                    look_directions,
                    combat_stats,
                    scores,
                    loots,
                    game_sprites,
                    enemy_markers,
                    npc_velocities,
                ),
                &ent,
                &mut markers,
                &mut alloc,
                &mut de,
            ).unwrap_or_else(|e| error!(self.logger, "Error deserializing savegame data: {}", e));
        }

        use ron::de::from_str;
        seed.0 = from_str(&lines[0]).expect("error deserialising seed");

        let new_current_map = from_str(&lines[1]).expect("error deserialising map id");

        if new_current_map != current_map.0 {
            if current_map.0 == 0 {
                game_state.step = Step::EnteringCave(new_current_map, None);
            } else {
                game_state.step = Step::EnteringSurface(Position { y: 0, x: 0 });
            }

            current_map.0 = new_current_map;
        }

        info!(self.logger, "Savegame loaded!");
    }
}

pub struct PostDeserialize {
    logger: Logger,
}
impl std::default::Default for PostDeserialize {
    fn default() -> Self {
        PostDeserialize {
            logger: ::utils::logger_from_config(),
        }
    }
}

impl<'a> System<'a> for PostDeserialize {
    type SystemData = (
        ReadStorage<'a, PlayerMarker>,
        ReadStorage<'a, Position>,
        Write<'a, GameState>,
        Option<Write<'a, PlayerEntity>>,
        Entities<'a>,
    );

    fn run(
        &mut self,
        (player_markers, positions, mut game_state, player_entity, entities): Self::SystemData,
    ) {
        debug!(self.logger, "Post deserialize!");
        let mut player_entity = match player_entity {
            Some(p) => p,
            None => panic!("no player entity after deserialisation!"),
        };

        for (_, entity) in (&player_markers, &*entities).join() {
            player_entity.0 = entity;
        }

        debug!(self.logger, "player entity: {:?}", player_entity.0);

        let player_position = *positions
            .get(player_entity.0)
            .expect("player has no position!");
        debug!(self.logger, "player position: {:?}", player_position);
        debug!(self.logger, "step: {:?}", game_state.step);
        match game_state.step {
            Step::EnteringCave(id, _) => {
                game_state.step = Step::EnteringCave(id, Some(player_position));
            }
            Step::EnteringSurface(_) => {
                game_state.step = Step::EnteringSurface(player_position);
            }
            _ => (),
        }
        debug!(self.logger, "Post deserialization finished!");
    }
}
