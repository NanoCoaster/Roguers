extern crate amethyst;

use amethyst::{
    ecs::{DispatcherBuilder, World},
    prelude::*,
    renderer::{Event, KeyboardInput, VirtualKeyCode, WindowEvent},
};
use components::*;
use resources::*;
use systems::*;

// TODO: This is pretty messy.
const SEED: [u8; 16] = [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

pub struct Roguers;

impl<'a, 'b> State<GameData<'a, 'b>> for Roguers {
    fn on_start(&mut self, data: StateData<GameData>) {
        register_components(data.world);
        register_resources(data.world);
        setup_player(data.world);
        setup_testing_environment(data.world);
    }

    fn handle_event(
        &mut self,
        _data: StateData<GameData>,
        event: Event,
    ) -> Trans<GameData<'a, 'b>> {
        match event {
            Event::WindowEvent { event, .. } => match event {
                WindowEvent::KeyboardInput {
                    input:
                        KeyboardInput {
                            virtual_keycode: Some(VirtualKeyCode::Escape),
                            ..
                        },
                    ..
                } => Trans::Quit,
                _ => Trans::None,
            },
            _ => Trans::None,
        }
    }

    fn update(&mut self, data: StateData<GameData>) -> Trans<GameData<'a, 'b>> {
        data.data.update(data.world);
        data.world.write_resource::<GameSystemState>().0 = false;
        data.world.maintain();

        Trans::None
    }
}

fn setup_player(world: &mut World) {
    let player_entity = world
        .create_entity()
        .with(DisplayName("Player".into()))
        .with(Position { x: 3, y: 3 })
        .with(LookDirection::North)
        .with(Health(20.0))
        .with(CombatStats {
            attack: 5.0,
            defense: 2.0,
        })
        .with(Score(0))
        .build();

    world.add_resource(PlayerEntity(player_entity));
}

fn setup_testing_environment(world: &mut World) {
    world
        .create_entity()
        .with(DisplayName("Enemy".into()))
        .with(Position { x: 5, y: 5 })
        .with(Health(20.0))
        .with(CombatStats {
            attack: 3.0,
            defense: 2.0,
        })
        .with(Loot(5))
        .with(CollidableMarker)
        .build();

    world
        .create_entity()
        .with(DisplayName("Chest".into()))
        .with(Position { x: 4, y: 3 })
        .with(CollidableMarker)
        .with(Health(5.0))
        .with(CombatStats {
            attack: 0.0,
            defense: 0.0,
        })
        .with(Loot(30))
        .build();
}

fn register_components(world: &mut World) {
    world.register::<Position>();
    world.register::<LookDirection>();
    world.register::<CollidableMarker>();
    world.register::<Score>();
    world.register::<DisplayName>();
}

/// Run all given systems once, each on a single dispatcher, calling `World.maintain()` and all the sync systems afterwards each time
macro_rules! run_systems_once {
    ($world:expr, $( $x: expr),* ) => {
        {
            use amethyst::shred::System;
            let mut position_sync_system = PositionLookupSyncSystem::default();
            position_sync_system.setup(&mut $world.res);

            let mut sync_dispatcher = DispatcherBuilder::new()
                // .with(CollidablesSyncSystem, "", &[])
                .with(position_sync_system, "", &[])
                .build();
            $(
                $world.write_resource::<GameSystemState>().0 = true;

                let mut dispatcher = DispatcherBuilder::new()
                    .with($x, "", &[])
                    .build();
                dispatcher.dispatch(&$world.res);
                &$world.maintain();

                sync_dispatcher.dispatch(&$world.res);
                &$world.maintain();

                $world.write_resource::<GameSystemState>().0 = false;
            )*
        }
    }
}

fn register_resources(world: &mut World) {
    // world.add_resource(CollisionPositions::default());
    world.add_resource(GameSystemState(false));

    let map = Map::generate_cave(50, 700, &SEED);

    world.add_resource(map);
    world.add_resource(PositionLookup::default());
    world.add_resource(InputCommandSet::default());

    run_systems_once!(
        world,
        // MapCollidablesSyncSystem,
        GenerateVegetationSystem,
        GenerateEnemiesSystem,
        GenerateChestsSystem
    );

    world.read_resource::<Map>().print();
}
