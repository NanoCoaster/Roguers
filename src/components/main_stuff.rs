use specs::prelude::*;

// TODO: use integers / floats? signed, unsigned?
/// A Position in the game world
#[derive(Debug, Default, Hash, PartialEq, Eq, Copy, Clone, Serialize, Deserialize)]
pub struct Position {
    pub x: i32,
    pub y: i32,
}
impl Component for Position {
    type Storage = FlaggedStorage<Self, VecStorage<Self>>;
}

/// Entities with this component have collision detection enabled
#[derive(Debug, Default, Copy, Clone, Serialize, Deserialize)]
pub struct CollidableMarker;
impl Component for CollidableMarker {
    type Storage = NullStorage<Self>;
}

/// Entities with this component have a primary facing direction
#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub enum LookDirection {
    North,
    East,
    South,
    West,
}
impl Component for LookDirection {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct Health(pub f32);
impl Component for Health {
    type Storage = DenseVecStorage<Self>;
}

/// Entities with this component can be attacked (but don't necesseraly attack themselves)
#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct CombatStats {
    pub attack: f32,
    pub defense: f32,
}
impl Component for CombatStats {
    type Storage = DenseVecStorage<Self>;
}

/// Game Score component
#[derive(Default, Copy, Clone, Debug, Serialize, Deserialize)]
pub struct Score(pub i32);
impl Component for Score {
    type Storage = HashMapStorage<Self>;
}

/// A Name for the entity that's human-friendly (but not necesseraly unique)
#[derive(Default, Debug)]
pub struct DisplayName(pub String);
impl Component for DisplayName {
    type Storage = VecStorage<Self>;
}

/// The amount of gold an entity carries
#[derive(Default, Copy, Clone, Debug, Serialize, Deserialize)]
pub struct Loot(pub i32);
impl Component for Loot {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug, Default, Copy, Clone, Serialize, Deserialize)]
pub struct PlayerMarker;
impl Component for PlayerMarker {
    type Storage = NullStorage<Self>;
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub enum GameSprite {
    Enemy,
    Player,
    Chest,
}
impl Component for GameSprite {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug, Default, Copy, Clone, Serialize, Deserialize)]
pub struct EnemyMarker;
impl Component for EnemyMarker {
    type Storage = NullStorage<Self>;
}

#[derive(Debug, Default)]
pub struct NpcActivities(pub Vec<NpcState>);
impl Component for NpcActivities {
    type Storage = HashMapStorage<Self>;
}

#[derive(Debug)]
pub enum NpcState {
    Roaming,
    Attacking,
}

#[derive(Debug, Default, Copy, Clone, Serialize, Deserialize)]
pub struct NpcVelocity(pub u32);
impl Component for NpcVelocity {
    type Storage = DenseVecStorage<Self>;
}
